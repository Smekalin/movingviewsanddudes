//
//  ViewController.m
//  UIViewAnimationsHomeWork
//
//  Created by Sergey on 24/08/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) NSArray* arraymovingViews;
@end

@implementation ViewController


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self addViewAnimation];
    
    for (int i = 0; i < 5; i++) {
        [self moveDude:[self createDude]];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView* view1 = [self addViewWithCoordinate:
                     CGPointMake(CGRectGetMinX(self.view.frame), CGRectGetMinY(self.view.frame))];
    
    UIView* view2 = [self addViewWithCoordinate:
                     CGPointMake(self.view.frame.size.width - 50, 0)];
    
    UIView* view3 = [self addViewWithCoordinate:
                     CGPointMake(self.view.frame.size.width - 50, self.view.frame.size.height - 50)];
    
    UIView* view4 = [self addViewWithCoordinate:
                     CGPointMake(0, self.view.frame.size.height - 50)];
    
    self.arraymovingViews = @[view1, view2, view3, view4];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Views functions

- (void) addViewAnimation{

    [UIView animateWithDuration:2
                          delay:0
                        options:(UIViewAnimationOptionCurveEaseInOut)
                     animations:^{
                         [self changeViews:self.arraymovingViews];
                         int random = arc4random()%2;
                         if (random) {
                             for (UIView* view in self.arraymovingViews) {
                                 view.transform = CGAffineTransformMakeRotation(0);
                                 view.transform = CGAffineTransformMakeRotation(M_PI_2);
                             }
                         }
                         else {
                             for (UIView* view in self.arraymovingViews) {
                                 view.transform = CGAffineTransformMakeRotation(0);
                                 view.transform = CGAffineTransformMakeRotation(-M_PI_2);
                             }
                         }
                     }
                     completion:^(BOOL finished) {
                         for (UIView* view in self.arraymovingViews) {
                             view.transform = CGAffineTransformMakeRotation(0);
                         }
                         [self addViewAnimation];
                     }];
}

- (UIColor*) randomColor {
    CGFloat r = (CGFloat)(arc4random() % 10) / 10;
    CGFloat g = (CGFloat)(arc4random() % 10) / 10;
    CGFloat b = (CGFloat)(arc4random() % 10) / 10;
    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}

- (UIView*) addViewWithCoordinate:(CGPoint) coord {
    
    CGRect rect = CGRectMake(coord.x, coord.y, 50, 50);
    
    UIView* view = [[UIView alloc] initWithFrame:rect];
    view.backgroundColor = [self randomColor];
    [self.view addSubview:view];
    return view;
}

- (void) changeViews:(NSArray*) arrayViews {
    
    CGPoint tmpCenter = [[arrayViews objectAtIndex:0] center];
    UIColor* tmpBackgroundColor = [[arrayViews objectAtIndex:0] backgroundColor];
    
    for (int i = 0; i < [arrayViews count] - 1; i++) {
        [[arrayViews objectAtIndex:i%4] setBackgroundColor:[[arrayViews objectAtIndex:(i+1)%4] backgroundColor]];
        [[arrayViews objectAtIndex:i%4] setCenter:[[arrayViews objectAtIndex:(i+1)%4] center]];
    }
    
    [[arrayViews objectAtIndex:([arrayViews count] - 1)] setBackgroundColor:tmpBackgroundColor];
    [[arrayViews objectAtIndex:([arrayViews count] - 1)] setCenter:tmpCenter];
}

#pragma mark - Dude functions

- (UIImageView*) createDude {
    
    UIImageView* imageView = [[UIImageView alloc] initWithFrame:CGRectMake(100, 100, 70, 100)];
    
    UIImage* rightFoot = [UIImage imageNamed:@"right.png"];
    UIImage* leftFoot = [UIImage imageNamed:@"left.png"];
    NSArray* images = @[rightFoot, leftFoot];
    
    imageView.animationImages = images;
    imageView.animationDuration = 0.5;
    [imageView startAnimating];
    [self.view addSubview:imageView];
    return imageView;
}
- (void) moveDude:(UIImageView*) dude {
    [UIView animateWithDuration:2
                          delay:0
                        options:(UIViewAnimationOptionCurveEaseOut)
                     animations:^{

                         int randX = (CGRectGetMinX(self.view.frame) + 50)
                                             + (arc4random()%(int)(CGRectGetMaxX(self.view.frame)
                                               - CGRectGetMinX(self.view.frame) - 100));
                         int randY = (CGRectGetMinY(self.view.frame) + 50)
                                             + (arc4random()%(int)(CGRectGetMaxY(self.view.frame)
                                               - CGRectGetMinY(self.view.frame) - 100));
                         dude.center = CGPointMake(randX, randY);
                     }
                     completion:^(BOOL finished) {
                         [self moveDude:dude];
                     }];
}

@end















